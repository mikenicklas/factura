require 'rails_helper'

describe CustomersController, type: :controller do
  let(:customer) { create(:customer) }
  let(:params) do
    {'customer' => {
      'name' => 'Google',
      'address' => '123 Main St',
      'phone_number' => '123-123-1234',
      'website_url' => 'http://google.com'}
    }
  end

  before(:each) { sign_in create(:user) }

  context 'POST /customers' do
    it 'redirects to index on success' do
      post :create, params: params
      expect(controller).to redirect_to customers_path
    end

    it 'renders new on failure' do
      params['customer']['name'] = nil
      post :create, params: params
      expect(controller).to render_template :new
    end
  end

  context 'GET /customers' do
    it 'renders index' do
      get :index
      expect(controller).to render_template :index
    end
  end

  context 'GET /customer/:id' do
    it 'renders show' do
      get :show, params: {id: create(:customer, user: controller.current_user).id}
      expect(controller).to render_template :show
    end
  end

end
