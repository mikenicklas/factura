require 'rails_helper'

describe OrganizationProfilesController, type: :controller do
  let(:params) do
    {'organization_profile' => {
      'name' => 'XYZ Corp',
      'address' => '123 Main St',
      'phone_number' => '123-123-1234',
      'website_url' => 'http://xyzcorp.com' }
    }
  end

  before(:each) do
    @user = create(:user)
    sign_in @user
  end

  it '#new' do
    get :new
    expect(controller).to render_template :new
  end

  it '#create' do
    post :create, params: params
    expect(controller).to redirect_to customers_path
  end
end
