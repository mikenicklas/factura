require 'rails_helper'

describe InvoicesController, type: :controller do
  let(:params) do
    {'invoice' => {
      'terms' => 'net_30',
      'due_date' => 7.days.from_now,
      'invoice_date' => Time.now}
    }
  end

  before(:each) do
    @user = create(:user)
    allow_any_instance_of(User).to receive(:organization_profile).and_return true
    sign_in @user
  end

  it 'requires an orgzation_profile' do
    allow_any_instance_of(User).to receive(:organization_profile).and_return false
    customer = create(:customer, user: @user)
    get :new, params: {'customer_id' => 1}
    expect(controller).to redirect_to new_organization_profile_path
  end

  context 'POST /customers/:id/invoices' do
    it 'redirects to index on success' do
      customer = create(:customer, user: @user)
      params['customer_id'] = customer.id
      post :create, params: params
      expect(controller).to redirect_to customer_path(customer.id)
    end

    it 'renders new on failure' do
      customer = create(:customer, user: @user)
      params['customer_id'] = customer.id
      allow_any_instance_of(Invoice).to receive(:save).and_return false
      post :create, params: params
      expect(controller).to render_template :new
    end
  end

  context 'GET /customers/:customer_id/invoices/:id' do
    it 'renders #show' do
      customer = create(:customer, user: @user)
      invoice = create(:invoice, customer: customer)
      get :show, params: {customer_id: customer.id, id: invoice.id}
      expect(controller).to render_template :show
    end
  end

end
