require 'rails_helper'

describe InvoiceDecorator do
  subject { InvoiceDecorator.decorate(create(:invoice)) }

  context 'with valid data' do
    it '#invoice_date_or_default' do
      expect(subject.invoice_date_or_default).to match /\w*\s\d*\,\s\d*/
    end

    it '#due_date_or_default' do
      expect(subject.due_date_or_default).to match /\w*\s\d*\,\s\d*/
    end
  end

  context 'without valid data' do
    it '#invoice_date_or_default' do
      allow(subject).to receive(:invoice_date).and_return nil
      expect(subject.invoice_date_or_default).to eql 'No Date Set'
    end

    it '#due_date_or_default' do
      allow(subject).to receive(:due_date).and_return nil
      expect(subject.due_date_or_default).to eql 'No Date Set'
    end
  end

end
