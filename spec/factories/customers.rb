FactoryGirl.define do
  factory :customer do
    name 'Google'
    address '111 Main St.'
    phone_number '222-222-2222'
    website_url 'http://google.com'
    user
  end
end
