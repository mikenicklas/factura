FactoryGirl.define do
  factory :line_item do
    name 'Consulting'
    description 'Hourly marketing consulting services'
    quantity 100
    rate 100
    invoice
  end
end
