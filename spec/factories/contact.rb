FactoryGirl.define do
  factory :contact do
    email 'admin@google.com'
    first_name 'John'
    last_name 'Doe'
    phone_number '123-123-1234'
    title 'VP of Sales'
    customer
  end
end
