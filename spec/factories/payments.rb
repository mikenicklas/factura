FactoryGirl.define do
  factory :payment do
    date Time.now
    amount 123.75
    payment_method 'check'
    memo 'Check #4123'
    invoice
  end
end
