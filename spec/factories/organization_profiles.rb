FactoryGirl.define do
  factory :organization_profile do
    name 'ABC Corp.'
    address '1 Main St.'
    phone_number '111-111-1111'
    website_url 'http://abcorp.com'
    user
  end
end
