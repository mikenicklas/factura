FactoryGirl.define do
  factory :invoice do
    due_date 7.days.from_now
    invoice_date 20.days.from_now
    terms 'net_30'
    notes 'Thank you for your business'
    customer
  end
end
