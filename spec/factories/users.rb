FactoryGirl.define do
  sequence :email do |n|
    "email#{n}@factory.com"
  end

  factory :user do
    email
    password 'testing123'
    password_confirmation 'testing123'

    after(:create) do |user|
      user.organization_profile = create(:organization_profile, user: user)
    end
  end
end
