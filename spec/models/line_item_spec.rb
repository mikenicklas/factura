require 'rails_helper'

RSpec.describe LineItem, type: :model do
  subject { create(:line_item) }

  it '#total' do
    expect(subject.total.to_i).to eql 10000
  end
end
