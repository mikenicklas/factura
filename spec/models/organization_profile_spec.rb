require 'rails_helper'

describe OrganizationProfile do
  subject { create(:organization_profile) }
  let(:required_attrs) { [:name, :address, :phone_number] }

  it 'is invalid without required attributes' do
    required_attrs.each do |attr|
      current_value = subject.send(attr)
      subject.send("#{attr}=", nil)
      expect(subject).to be_invalid
      subject.send("#{attr}=", current_value)
    end
  end

end
