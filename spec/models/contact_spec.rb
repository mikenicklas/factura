require 'rails_helper'

describe Contact do
  subject { create(:contact) }
  let(:required_attrs) { [:first_name, :last_name, :email] }

  it 'is invalid without required attributes' do
    required_attrs.each do |attr|
      current_value = subject.send(attr)
      subject.send("#{attr}=", nil)
      expect(subject).to be_invalid
      subject.send("#{attr}=", current_value)
    end
  end


end
