require 'rails_helper'

describe Customer do
  subject { create(:customer) }

  it 'requires a name' do
    subject.name = nil
    expect(subject).to be_invalid
  end

  it 'name should be unique scoped to user' do
    duplicate_customer = subject.dup
    expect(duplicate_customer).to be_invalid
    duplicate_customer.user = create(:user, email: 'somebodynew@gmail.com')
    expect(duplicate_customer).to be_valid
  end

  it '#total_billed' do
    allow(subject).to receive(:invoices).and_return [create(:invoice, customer: subject), create(:invoice, customer: subject)]
    allow_any_instance_of(Invoice).to receive(:amount_due).and_return 100
    expect(subject.total_billed.to_i).to eql 200
  end

end
