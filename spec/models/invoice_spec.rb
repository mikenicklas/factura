require 'rails_helper'

describe Invoice do
  subject { create(:invoice) }
  let(:required_attrs) { [:terms, :due_date] }

  it 'is invalid without required attributes' do
    required_attrs.each do |attr|
      current_value = subject.send(attr)
      subject.send("#{attr}=", nil)
      expect(subject).to be_invalid
      subject.send("#{attr}=", current_value)
    end
  end

  it '#amount_due' do
    line_item_one = subject.line_items.create(quantity: 1, rate: 100)
    line_item_two = subject.line_items.create(quantity: 3, rate: 75)
    expect(subject.amount_due.to_i).to eql 325
  end

  it '#status' do
    subject.status = Invoice.statuses[:paid]
    expect(subject.status).to eql 'paid'
  end

  it '#terms' do
    subject.terms = Invoice.terms[:net_30]
    expect(subject.terms).to eql 'net_30'
  end

  it '#total_paid' do
    subject.payments << create(:payment, amount: 10)
    subject.payments << create(:payment, amount: 20)
    expect(subject.total_paid).to eql 30.0
  end

  it '#paid_in_full?' do
    subject.payments << create(:payment, amount: subject.amount_due)
    expect(subject.paid_in_full?).to eql true
  end

  context '#update_status' do
    it 'when paid in full' do
      allow_any_instance_of(Invoice).to receive(:amount_due).and_return 100
      subject.payments << create(:payment, amount: 100)
      subject.update_status
      expect(subject.status).to eql 'paid'
    end

    it 'when partially paid' do
      allow_any_instance_of(Invoice).to receive(:amount_due).and_return 100
      subject.payments << create(:payment, amount: 90)
      subject.update_status
      expect(subject.status).to eql 'partially_paid'
    end

    it 'when unpaid' do
      allow_any_instance_of(Invoice).to receive(:amount_due).and_return 100
      allow(subject).to receive(:payments).and_return []
      subject.update_status
      expect(subject.status).to eql 'unpaid'
    end
  end

end
