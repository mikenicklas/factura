require 'rails_helper'

describe Payment do
  subject { create(:payment) }

  let(:required_attrs) { [:amount, :date, :payment_method] }

  it 'is invalid without required attributes' do
    required_attrs.each do |attr|
      current_value = subject.send(attr)
      subject.send("#{attr}=", nil)
      expect(subject).to be_invalid
      subject.send("#{attr}=", current_value)
    end
  end

end
