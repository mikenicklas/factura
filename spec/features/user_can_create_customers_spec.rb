require 'rails_helper'

RSpec.feature 'UserCanCreateCustomers', type: :feature do
  before(:each) do
    @user = create(:user)
  end

  it 'can create a customer with nested contact' do
    sign_in @user
    visit root_path
    click_on 'Customers'
    click_on 'New Customer'
    fill_in 'customer[name]', with: 'Google'
    fill_in 'customer[address]', with: '123 Main St'
    fill_in 'customer[website_url]', with: 'http://google.com'
    fill_in 'customer[phone_number]', with: '123-123-1234'
    fill_in 'customer[contacts_attributes][0][first_name]', with: 'John'
    fill_in 'customer[contacts_attributes][0][last_name]', with: 'Doe'
    fill_in 'customer[contacts_attributes][0][email]', with: 'jdoe@gmail.com'
    fill_in 'customer[contacts_attributes][0][phone_number]', with: '123-123-1234'
    fill_in 'customer[contacts_attributes][0][title]', with: 'Regional Sales Manager'
    click_button 'Create Customer'
    expect(page).to have_content 'successfully'
    expect(@user.customers.last.contacts.size).to eql 1
  end
end
