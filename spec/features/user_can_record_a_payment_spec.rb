require 'rails_helper'

RSpec.feature 'UserCanCreateCustomers', type: :feature do
  before(:each) do
    @user = create(:user)
  end

  it 'can record a payment' do
    customer = create(:customer, user: @user)
    customer.invoices << create(:invoice, customer: customer)
    sign_in @user
    visit customer_invoice_path(customer_id: customer.id, id: customer.invoices.first.id)
    click_on 'Record Payment'
    fill_in 'payment[date]', with: Time.now.to_s
    fill_in 'payment[amount]', with: '100'
    select 'Wire transfer', from: 'payment_payment_method'
    fill_in 'payment[memo]', with: 'via PayPal'
    click_on 'Record Payment'
    expect(page).to have_content 'successfully'
  end

end
