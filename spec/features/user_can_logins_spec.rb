require 'rails_helper'

RSpec.feature "UserCanLogin", type: :feature do
  before(:each) do
    @user = User.create!( email: 'test@example.com',
                          password: 'testing123',
                          password_confirmation: 'testing123' )
  end

  it 'redirects to dashboard' do
    visit root_path
    fill_in 'user[email]', with: @user.email
    fill_in 'user[password]', with: @user.password
    click_button 'Log in'
    expect(page).to have_content 'Overview'
  end

  it 'has signout link' do
    sign_in @user
    visit root_path
    click_on 'Sign Out'
    expect(page).to have_content 'Log in'
  end
end
