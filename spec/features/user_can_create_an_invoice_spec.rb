require 'rails_helper'

RSpec.feature 'UserCanCreateCustomers', type: :feature do
  before(:each) do
    @user = create(:user)
  end

  it 'can create an invoice with nested line item' do
    customer = create(:customer, user_id: @user.id)
    sign_in @user
    visit customer_path(customer.id)
    click_on 'New Invoice'
    fill_in 'invoice[invoice_date]', with: Time.now
    fill_in 'invoice[due_date]', with: 30.days.from_now
    select 'Net 30', from: 'invoice_terms'
    fill_in 'invoice[notes]', with: 'Thank you for your business'
    fill_in 'invoice[line_items_attributes][0][name]', with: 'Consulting'
    fill_in 'invoice[line_items_attributes][0][quantity]', with: '2'
    fill_in 'invoice[line_items_attributes][0][rate]', with: '100'
    click_button 'Create Invoice'
    expect(page).to have_content 'successfully'
  end

  it 'can view previously created invoices' do
    customer = create(:customer, user_id: @user.id)
    invoice = create(:invoice, customer_id: customer.id)
    sign_in @user
    visit customer_path(customer.id)
    click_on 'View'
    expect(page).to have_content "#{customer.name} / Invoice"
  end

  it 'can edit an invoice' do
    customer = create(:customer, user_id: @user.id)
    invoice = create(:invoice, customer_id: customer.id)
    sign_in @user
    visit customer_path(customer.id)
    click_on 'Edit'
    fill_in 'invoice[due_date]', with: 100.days.from_now
    click_on 'Update'
    expect(page).to have_content 'success'
  end
end
