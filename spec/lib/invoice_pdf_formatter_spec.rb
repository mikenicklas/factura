require 'rails_helper'

describe InvoicePdfFormatter do
  let(:invoice) { create(:invoice) }
  subject { InvoicePdfFormatter }

  it '#create_template returns string of template' do
    templ = subject.create_template(invoice)
    expect(templ).to be_truthy
  end
end
