require 'rails_helper'

describe InvoicePdfGenerator do
  let(:invoice) { create(:invoice) }
  let(:line_item) { create(:line_item, invoice: invoice) }

  subject { InvoicePdfGenerator.new(invoice) }

  it '#create_pdf' do
    invoice.line_items << line_item
    expect(PDFKit).to receive(:new).and_call_original
    subject.create_pdf
  end
end
