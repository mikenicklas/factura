class OrganizationProfilesController < BaseController
  def new
    @profile = current_user.build_organization_profile
  end

  def create
    @profile = current_user.build_organization_profile(profile_params)
    if @profile.save
      redirect_to customers_path
    else
      render :new
    end
  end

  private

  def profile_params
    params.require(:organization_profile).permit(:name, :address, :phone_number, :website_url)
  end
end
