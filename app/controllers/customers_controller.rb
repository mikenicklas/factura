class CustomersController < BaseController
  def index
    @customers = CustomerDecorator.decorate_collection(current_user.customers)
  end

  def new
    @customer = current_user.customers.new
    @customer.contacts.new
  end

  def create
    @customer = current_user.customers.new(customer_params)
    if @customer.save
      flash[:success] = "You have successfully added #{@customer.name}"
      redirect_to customers_path
    else
      render :new
    end
  end

  def show
    @customer = CustomerDecorator.decorate(current_customer)
  end

  private

  def customer_params
    params.require(:customer).permit(:name, :address, :phone_number, :website_url,
      contacts_attributes: [:first_name, :last_name, :email, :title, :phone_number])
  end

  def current_customer
    current_user.customers.find(params[:id])
  end
end
