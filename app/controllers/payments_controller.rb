class PaymentsController < BaseController
  def new
    @payment = PaymentDecorator.decorate(current_invoice.payments.new)
  end

  def create
    @nps = NewPaymentService.new(user: current_user, params: params)
    if @nps.call
      flash[:success] = 'Your payment has beeen successfully applied!'
      redirect_to customer_invoice_path(customer_id: params[:customer_id], id: params[:invoice_id])
    else
      render :new
    end
  end

  private

  def current_customer
    current_user.customers.find_by_id(params[:customer_id])
  end

  def current_invoice
    current_customer.invoices.find_by_id(params[:invoice_id])
  end
end
