class InvoicesController < BaseController
  before_action :ensure_organization_profile_exists

  def new
    @invoice = current_customer.invoices.new
    @invoice.line_items.new
  end

  def create
    @invoice = current_customer.invoices.new(invoice_parameters)
    if @invoice.save
      flash[:success] = 'Your invoice was created successfully.'
      redirect_to customer_path(@invoice.customer.id)
    else
      render :new
    end
  end

  def show
    @invoice = InvoiceDecorator.decorate(current_invoice)
  end

  def edit
    @invoice = current_invoice
  end

  def update
    @invoice = current_invoice
    if @invoice.update_attributes(invoice_parameters)
      flash[:success] = 'Your invoice has been successfully updated.'
      redirect_to customer_path(current_customer)
    else
      render :edit
    end
  end


  private

  def invoice_parameters
    params.require(:invoice).permit(:invoice_date, :due_date, :terms, :notes, :adjustment_percentage,
      line_items_attributes: [:name, :description, :quantity, :rate, :id])
  end

  def current_customer
    current_user.customers.find(params[:customer_id])
  end

  def current_invoice
    current_customer.invoices.find(params[:id])
  end

  def ensure_organization_profile_exists
    unless current_user.organization_profile
      redirect_to new_organization_profile_path
    end
  end
end
