class InvoicePdfGenerator
  attr_reader :invoice

  def initialize(invoice)
    @invoice = invoice
  end

  def create_pdf
    kit.to_pdf
    kit.to_file "#{Rails.root}/public/invoices/#{filename}.pdf"
  end

  private

  def kit
    @generator ||= PDFKit.new(template, page_size: 'Letter')
  end

  def template
    @template ||= InvoicePdfFormatter.create_template(invoice)
  end

  def filename
    "#{invoice.customer.user.id}-#{invoice.customer.id}-#{invoice.id}"
  end

end
