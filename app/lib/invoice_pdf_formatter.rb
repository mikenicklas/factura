class InvoicePdfFormatter
  attr_reader :invoice

  def initialize(invoice)
    @invoice = invoice
  end

  def template
    decorated = InvoiceDecorator.decorate(invoice)
    render_template(decorated)
  end

  def render_template(invoice)
    template = Slim::Template.new("#{Rails.root}/app/views/invoices/pdf.html.slim")
    template.render(invoice)
  end

  class << self
    def create_template(invoice)
      new(invoice).template
    end
  end

end
