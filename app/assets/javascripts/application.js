// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require invoice-totaler
//= require_tree .

Turbolinks.start()

$(document).on('turbolinks:load', function() {
  var sidebarToggles = $('.sidebar-toggle-btn');

  sidebarToggles.on('click', function(e) {
    e.preventDefault();
    $('.sidebar').toggleClass('closed open');
    $('.sidebar-overlay').toggleClass('show');
  });

  $('form').on('click', '.add-nested-attribute-fields', function(e) {
    e.preventDefault();
    var time = new Date().getTime(),
        regexp = new RegExp($(this).data('id'), 'g'),
        event = jQuery.Event('factura:line-item-added');

    $(this).before($(this).data('fields').replace(regexp, time))

    event.lineItem = $(this).prev();
    $(document).trigger(event)
  });
});

(function($) {
  $.fn.currencyInput = function() {
    this.each(function() {
      var wrapper = $("<div class='input-group' />");
      $(this).wrap(wrapper);
      $(this).before("<span class='input-group-addon'>$</span>");
      $(this).change(function() {
        var min = parseFloat($(this).attr("min"));
        var max = parseFloat($(this).attr("max"));
        var value = this.valueAsNumber;
        if(value < min)
          value = min;
        else if(value > max)
          value = max;
        $(this).val(value.toFixed(2));
      });
    });
  };
})(jQuery);
