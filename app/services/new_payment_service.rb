class NewPaymentService
  attr_accessor :user, :params

  def initialize(user:, params:)
    @user = user
    @params = params
  end

  def call
    payment = build_payment
    payment.save ? update_invoice_status : false
  end

  private

  def customer
    user.customers.find_by_id(params[:customer_id])
  end

  def invoice
    customer.invoices.find_by_id(params[:invoice_id])
  end

  def build_payment
    invoice.payments.new do |p|
      p.amount = payment_params[:amount]
      p.date = payment_params[:date]
      p.memo = payment_params[:memo]
      p.payment_method = payment_params[:payment_method]
    end
  end

  def payment_params
    params[:payment]
  end

  def update_invoice_status
    invoice.update_status
    true
  end

end
