class Invoice < ApplicationRecord
  belongs_to :customer
  has_many :line_items
  has_many :payments

  validates :due_date, presence: true
  validates :terms, presence: true
  validates_associated :line_items

  accepts_nested_attributes_for :line_items

  enum status: {paid: 1, unpaid: 2, partially_paid: 3}
  enum terms: {due_on_receipt: 1, net_7: 2, net_15: 3, net_30: 4, net_45: 5, end_of_month: 6}

  def amount_due
    self.line_items.inject(0.0) do |sum, line_item|
      line_item.total + sum
    end
  end

  def total_paid
    self.payments.inject(0.0) do |total_paid, payment|
      payment.amount + total_paid
    end
  end

  def paid_in_full?
    total_paid >= amount_due
  end

  def partially_paid?
    total_paid > 0 && !paid_in_full?
  end

  def update_status
    if paid_in_full?
      self.status = :paid
    elsif partially_paid?
      self.status = :partially_paid
    else
      self.status = :unpaid
    end
    self.save
  end

end
