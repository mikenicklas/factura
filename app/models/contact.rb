class Contact < ApplicationRecord
  belongs_to :customer, optional: true

  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, presence: true

end
