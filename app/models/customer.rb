class Customer < ApplicationRecord
  belongs_to :user
  has_many :contacts
  has_many :invoices

  accepts_nested_attributes_for :contacts

  validates :name, presence: true, uniqueness: {scope: :user}
  validates_associated :contacts

  def total_billed
    self.invoices.inject(0.0) do |sum, invoice|
      invoice.amount_due + sum
    end
  end

end
