class Payment < ApplicationRecord
  belongs_to :invoice

  validates :amount, presence: true
  validates :date, presence: true
  validates :payment_method, presence: true

  enum payment_method: {cash: 0, credit_card: 1, debit_card: 2, check: 3, wire_transfer: 4, other: 5}

end
