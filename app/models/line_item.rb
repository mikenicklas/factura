class LineItem < ApplicationRecord
  belongs_to :invoice, optional: true

  def total
    self.quantity * self.rate
  end
end
