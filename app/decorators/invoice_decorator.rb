class InvoiceDecorator < Draper::Decorator
  include ActionView::Helpers::NumberHelper
  delegate_all

  def invoice_date_or_default
    invoice_date ? dateify(invoice.invoice_date) : 'No Date Set'

  end

  def due_date_or_default
    due_date ? dateify(invoice.due_date) : 'No Date Set'
  end

  def notes_or_default
    notes ? notes : 'No notes provided'
  end

  def terms_or_default
    terms ? terms : 'No terms provided'
  end

  def dateify(datetime)
    datetime.to_datetime.strftime('%b %e, %Y')
  end

  def customer_name
    self.customer.name
  end

end
