class PaymentDecorator < Draper::Decorator
  delegate :url_helpers, to: 'Rails.application.routes'
  delegate_all

  def create_path
    url_helpers.customer_invoice_payments_path(customer_id: customer.id, invoice_id: self.invoice.id)
  end

  def customer
    self.invoice.customer
  end

end
