class CustomerDecorator < Draper::Decorator
  delegate_all
  decorates_association :invoices
end
