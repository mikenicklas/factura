(function(jQuery) {

  var InvoiceTotaler = function(opts) {
    this.options = opts;
    this.$el = opts.$el;
    this.triggerEls = ['.quantity', '.currency'];
    this.$triggers = this.$el.find(this.triggerEls.join(', '));
    this.currentInvoiceTotal = 0;
    this.bind();
    return this;
  }

  InvoiceTotaler.prototype = {
    bind: function() {
      var self = this;
      this.calculate();
      this.$triggers.on('change', function() {
        self.calculate();
      });
      this.setRebindListener();
    },

    rebind: function() {
      var triggers = this.triggerEls.join(', '),
          $triggers = this.$el.find(triggers),
          self = this;

      $triggers.off('change');
      $triggers.on('change', function() {
        self.calculate();
      });
    },

    calculate: function() {
      var lineItemRows = $('.line-item-row'),
          self = this;
      this.currentInvoiceTotal = 0;
      $.each(lineItemRows, function() {
        var quantity = $(this).find('.quantity').val(),
            rate = $(this).find('.currency').val(),
            lineItemTotal = quantity * rate;
         self.currentInvoiceTotal += lineItemTotal;
      });

      this.displayTotal();
    },

    displayTotal: function() {
      $('#invoice-value-number').html('$' + this.currentInvoiceTotal);
    },

    setRebindListener: function() {
      var rebindOn = this.options.rebindOn,
          self = this;
      if(rebindOn) {
        $(document).on(rebindOn, function() {
          self.rebind();
        })
      }
    },

    currentTotal: function() {
      return this.currentInvoiceTotal;
    }
  };

  jQuery.fn.invoiceTotaler = function(options) {
    var opts = options || {};

    for(var i = 0; i < this.length; i++) {
      if($(this).data('invoice-totaler')) {
        return $(this).data('invoice-totaler')[options]();
      } else {
        options.$el = $(this);
        $(this).data('invoice-totaler', new InvoiceTotaler(opts));
      }
    }
  }
})(jQuery);
