Rails.application.routes.draw do
  devise_for :users
  resources :organization_profiles, only: [:new, :create]

  root 'dashboard#show'

  resources :customers, only: [:index, :new, :create, :show] do
    resources :invoices do
      resources :payments, only: [:new, :create]
    end
  end
end
