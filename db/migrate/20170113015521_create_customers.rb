class CreateCustomers < ActiveRecord::Migration[5.0]
  def change
    create_table :customers do |t|
      t.integer :user_id
      t.string :name
      t.string :address
      t.string :phone_number
      t.string :website_url

      t.timestamps
    end
  end
end
