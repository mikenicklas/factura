class CreateInvoices < ActiveRecord::Migration[5.0]
  def change
    create_table :invoices do |t|
      t.integer :customer_id
      t.string :terms
      t.timestamp :due_date
      t.string :notes

      t.timestamps
    end
  end
end
