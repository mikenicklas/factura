class AddColumnsToInvoices < ActiveRecord::Migration[5.0]
  def change
    add_column :invoices, :invoice_date, :datetime
    add_column :invoices, :adjustment_percentage, :string
  end
end
