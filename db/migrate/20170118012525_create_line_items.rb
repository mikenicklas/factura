class CreateLineItems < ActiveRecord::Migration[5.0]
  def change
    create_table :line_items do |t|
      t.integer :invoice_id
      t.string :name
      t.string :description
      t.float :quantity
      t.float :rate

      t.timestamps
    end
  end
end
