class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.integer :invoice_id
      t.float :amount
      t.integer :payment_method
      t.datetime :date
      t.string :memo

      t.timestamps
    end
  end
end
