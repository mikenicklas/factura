class CreateContacts < ActiveRecord::Migration[5.0]
  def change
    create_table :contacts do |t|
      t.integer :customer_id
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.string :title

      t.timestamps
    end
  end
end
