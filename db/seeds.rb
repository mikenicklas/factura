user = User.create!(email: 'test@example.com', password: 'testing123', password_confirmation: 'testing123')
customer = user.customers.create!(name: 'Google', address: '123 Main St', phone_number: '123-123-1234')
invoice = customer.invoices.create!(due_date: 30.days.from_now, terms: 'Net 30')
